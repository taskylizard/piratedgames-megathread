## piratedgames-megathread - [Moniteur de Statut](https://taskylizard.github.io/probable-chainsaw)

**Voici le Mega Thread de r/PiratedGames!**

## **Sites de Torrents:**

- **Choix des Moderateurs: [RARBG](https://rarbg.to)** - Torrents sûrs, meilleure qualité.

Autres mentions:

- [1337X](https://1337.to/) - Beaucoup de contenu avec une bonne équipe de modération.
- [Mac Torrents](https://mac-torrents.io/mac-games/) - Torrents pour Mac.
- [NXMAC](https://nxmac.com/) - Jeux Macs.
- [RuTor](http://www.rutor.info/) - Trackeurs de torrents Russe.
- [RuTracker](https://rutracker.org/) - Forums de torrents Russe.
- [Rustorka](http://rustorka.com/) - Trackeur de torrents Russe.
- [Tapochek](https://tapochek.net/index.php) - Tracker de torrents Russe.
- [Torrents.csv](https://torrents-csv.ml/) - Moteur de recherche pour torrents.

**Nous ne recommendons pas The Pirate Bay ou Kickass Torrents car il est très facile de télécharger des virus.**

## **Sites de Téléchargement Direct:**

- **Choix des Moderateurs: [CS.RIN.RU - Steam Underground Community](https://cs.rin.ru/forum/)** - Meilleur endroit pour n'importe quel jeux, ce site devrait être le premier endroit que vous vérifiez pour un jeux. N'autorise pas les publications de groupes de crack.

**Autres Mentions:**

- [CrackHub](https://crackhub.site/) - Site de téléchargement pour des jeux de groupes de crack et pour les repacks FitGirl.
- [Crackhub [Scene Games]](https://scene.crackhub.site/) - Vous emmene directement au uploads de Scène de CrackHub.
- [GLOAD](https://gload.cc/) - Site de téléchargement Allemand.
- [Gamesdrive.net](https://gamesdrive.net/) - Repacks et des Jeux de Scène.
- [GOG Games](https://gog-games.com/) - Jeux GOG.
- [Mobilisim](https://forum.mobilism.org/index.php) - Applications et jeux mobiles.
- [MyAbandonware](https://www.myabandonware.com/) - Jeux vieux/retro.
- [Online-fix.me](https://online-fix.me/) - Jeux Multijoueurs en-ligne.
- [Ovagames](http://www.ovagames.com/) - Téléchargement de Jeux.
- [RLSBB](https://rlsbb.ru/) - Téléchargement de jeux de Scène.
- [SCNLOG.ME](https://scnlog.me/) - Téléchargement de jeux de Scène.
- [SteamUnlocked](https://steamunlocked.net/)
- [Torrminatorr Forum](https://forum.torrminatorr.com/) - Jeux en téléchargement pour les jeux de Scène et GOG.

Les utilisateurs d'Oculus Quest peuvent télécharger des jeux de [ce thread](https://cs.rin.ru/forum/viewtopic.php?f=38&t=103122) sur les forums de [CS.RIN.RU](http://cs.rin.ru/).
[Cette publication](https://cs.rin.ru/forum/viewtopic.php?p=2077062#p2077062) est aussi utile.

**Pour du contenu de Crack seulement, NoCD et trainers, allez à [GameCopyWorld](https://gamecopyworld.com/games/index.php).**

## **Repacks**

Les repacks sont des versions très compréssées de jeux, faites pour des perssonnes avec une connection Internet lente/limitée. Les repackers listés ci-dessous sont des repackers sûrs et sans virus (malware).

- [BlackBox](http://www.blackboxrepack.com/)
- [Chovka](https://repack.info/)
- [CPG Repacks](http://cpgrepacks.site/)
- [Dodi](http://dodi-repacks.site/)
- [Darck](https://darckrepacks.com/)
- [ElAmigos](https://elamigos.site/)
- [FitGirl](http://fitgirl-repacks.site/)
- [Gnarly](https://gnarly-repacks.site/)
- [KaosKrew](https://kaoskrew.org/)
- [Kapital Sin](http://www.kapitalsin.com/forum/)
- [M4ckDOge](https://m4ckd0ge-repacks.me/)
- [Masquerade](https://masquerade.site/)
- MJ DJ
- R.G. Catalyst
- R.G. Mechanics
- R.G. Revenants
- [Scooter Repacks](https://scooter-repacks.site/)
- [Xatab](https://xatab-repack.com/)
- ZAZIX

## **Réseaux de Publications**

Veuillez noter que ces sites ne fournissent pas de téléchargements, seulement des informations sur les sorties de Scène/P2P

- **Choix des Moderateurs: [r/CrackWatch](https://reddit.com/r/crackwatch)** - Droit au but pour les Jeux-vidéos.

Autres mentions:

- [predb.me](https://predb.me/)
- [predb.org](https://predb.org/)
- [predb.ovh](https://predb.ovh/)
- [predb.pw](https://predb.pw/)
- [xREL](https://xrel.to/)

Veuillez noter que le site "crackwatch" n'est pas recommendé car il n'est pas officiel.

## **Sites de ROM:**

- [CDRomance](https://cdromance.com/)
- [Edge Emulation](https://edgemu.net/)
- [Emu Paradise](https://www.emuparadise.me/) - [Utiliez ce script](https://www.reddit.com/r/Piracy/comments/968sm6/a_script_for_easy_downloading_of_emuparadise_roms/) pour télécharger.
- [The Eye](http://the-eye.eu/public/rom/)
- [TheRomDepot](https://theromdepot.com/)
- [Romulation](https://www.romulation.net/)
- [r/ROMS](https://reddit.com/r/roms)
- [Vimm's Lair](https://vimm.net/?p=vault)
- [Ziperto](https://www.ziperto.com/) - Site de téléchargement pour ROM PlayStation et Nintendo.

## **Logiciels de Torrents:**

Pour télécharger des torrents, utilisez ces logiciels:

- **Choix des Moderateurs: [Qbittorrent](https://www.qbittorrent.org/download.php)**

Autres mentions:

- [BiglyBT](https://www.biglybt.com/download/)
- [Deluge](https://dev.deluge-torrent.org/wiki/Download)
- [PicoTorrent](https://picotorrent.org/download/)
- [Transmission](https://transmissionbt.com/download/)

## **Logiciels de Téléchargements Directs:**

Pour télécharger directement, utilisez ces logiciels:

- **Choix des Moderateurs: [JDownloader2](http://jdownloader.org/jdownloader2)**

Autres mentions:

- [FreeDownloadManager](https://www.freedownloadmanager.org/)
- [Internet Download Manager.](https://www.internetdownloadmanager.com/download.html)
- [Motrix](https://motrix.app/)
- [pyLoad](https://pyload.net/)
- [Xtreme Download Manager](https://subhra74.github.io/xdm/)

## **Services de Debride**

Un service de debride va convertir des lien de téléchargements premium/lents en lien de téléchargements plus rapides, juste pour vous.

- [Real Debrid](https://real-debrid.com/)
- [AllDebrid](https://alldebrid.com/)
- [Premiumize](https://www.premiumize.me/)

## **Outils**

Ces outils sont utiles pour cracker vos propres jeux.

- [ALi213 Steam Emulator](https://mega.nz/file/0lYGhDBb#4-x-4oPliA4ZR-mw4yAE3FzM5kzV-4xSVvK3JAYOFkQ) - Emulez Steam.
- [Auto CreamAPI](https://cs.rin.ru/forum/viewtopic.php?p=2013521#p2013521) - Configure automatiquement votre jeux pour fonctionner avec CreamAPI.
- [Auto Steamworks Fix Tool](https://cs.rin.ru/forum/viewtopic.php?f=29&t=97112) - Creez des fix pour Steamworks.
- [CODEX Steam Emulator](https://mega.nz/file/coQwTLLT#xx9gNuah5LQ1pkIeEf3UiQTCksaK7694fdXODmZgCZ0) - Emulez Steam.
- [CreamAPI](https://cs.rin.ru/forum/viewtopic.php?f=29&t=70576) - Débloquez des DLC piratés sur des Jeux Steam légitimes.
- [GoldBerg Emulator](https://cs.rin.ru/forum/viewtopic.php?f=29&t=91627) - Emulez Steam
- [Lucky Patcher](https://www.luckypatchers.com/) - Patchez des apps Android (meilleur avec root)
- [LumaPlay](https://cs.rin.ru/forum/viewtopic.php?f=29&t=67197) - Emulez Uplay
- [Nemirtingas Epic Emulator](https://cs.rin.ru/forum/viewtopic.php?f=29&t=105551)
- [Origin DLC unlocker](https://cs.rin.ru/forum/viewtopic.php?f=20&t=104412) - Débloquez des DLC piratés sur des jeux Origin légitimes. Testé seulement sur Les Sims 4.
- [RIN SteamInternals - A Broad Collection of Steam Tools](https://cs.rin.ru/forum/viewtopic.php?f=10&t=65887)
- [SmartSteamEmu](https://cs.rin.ru/forum/viewtopic.php?f=29&t=62935) - Emulez Steam
- [Steamless](https://cs.rin.ru/forum/viewtopic.php?f=29&t=88528) - Enlevez le DRM Steam de fichiers EXE (utilisé avec un emulateur Steam)

## **Logiciels Utiles:**

- [7zip File Manager](https://www.7-zip.org/) - Meilleur que WinRAR, open source.
- [Achievement Watcher](https://github.com/xan105/Achievement-Watcher/) - Visualisez tous les succès sur votre PC même s'il vient de Steam, d'un emulateur Steam et plus encore.
- [BleachBit](https://www.bleachbit.org/) - Nettoyeur Système, plus sûr que CCleaner.
- [Bitwarden](https://bitwarden.com/) - Gestionnaire de Mot de passe Open-Source.
- [QuickSFV](https://www.quicksfv.org/download.html) - Verifiez des checksums et générez des informations sur des hash.
- [WinRAR](https://www.rarlab.com/) - Alternative à 7zip.
- [Vapor Store](https://github.com/CrypticShy/vapor-store) - Un store (similaire a Steam!) où vous pouvez télécharger des jeux crackés!

**Pour des Produits Microsoft (Windows/Office) vous pouvez utiliser [MAS](https://github.com/massgravel/Microsoft-Activation-Scripts/releases) pour les activer.**

**Pour des produits Adobe, visitez [monkrus](http://monkrus.ws/).**

## **Extensions de Naviguateur Utiles:**

Ces extensions n'aident pas beaucoup pour le piratage en-ligne, mais vont rendre votre naviguation beaucoup plus sûre et respectueuse de votre vie privée.

- [Decentraleyes](https://git.synz.io/Synzvato/decentraleyes)/[LocalCDN](https://www.localcdn.org/)
- [HTTPS Everywhere](https://www.eff.org/https-everywhere)
- [Privacy Badger](https://privacybadger.org/)
- Trace
- [Ublock Origin](https://github.com/gorhill/uBlock)
- [Universal Bypass](https://universal-bypass.org/)

## **DNS:**

En utilisant un DNS axé sur la protection de la vie privée, vous pouvez accélérer votre experience de naviguation et rendre votre traffic Internet plus sûr. (N'EST PAS UN REMPLACEMENT A UN VPN):

- **Choix des Moderateurs: [Adguard DNS](https://adguard.com/en/adguard-dns/overview.html)**

Plus de fournisseurs DNS peuvent être trouvés [ici](https://www.privacytools.io/providers/dns/)

## **VPNs:**

- Il ets très difficile de recommander un service VPN. C'est pourquoi nous vous invitons a poser vos question sur [r/VPN](http://reddit.com/r/vpn).

**Tor n'est PAS un VPN, il ne vous protégera PAS quand vos téléchargez des Torrents!**

## **Logiciels non sécurisés:**

- Bittorrent - Meme chose que Utorrent, a des pubs, trackers et est non securisé.
- Utorrent - Meme chose que Bittorrent, a des pubs, trackers et est non securisé.
- CCleaner - Appartient a Avast.
- Avast - Connu pour la collection de données et la revente de celles-ci.

## **Sites non Fiables:**

- IGG Games/ PC Games Torrents - Ils ont [doxxé](https://fr.wikipedia.org/wiki/Divulgation_de_donn%C3%A9es_personnelles) mercs213 (propriétaire de GoodOldDownloads), emballent leur propre DRM dans des jeux indés et vous exploite pour l'argent des pubs.
- BBRepacks - Copie de BlackBox Repacks, contient des virus.
- OceanOfGames - Risque de virus élevé.
- nosTEAM
- Seyter - Les repacks contiennent des miners de cryptomonaie.
- xGIROx - Les repacks contiennent des miners de cryptomonaie.
- cracked-games.org - Risque de virus.
- crackingpatching - Risque de virus.
- Pirate Bay - Risque de virus.
- Kickass Torrents - Risque de virus.
- N'importe quel site qui contient un nom de groupe de crack dans l'URL - **LES GROUPES DE CRACK N'ONT PAS DE SITES**

Quiconque se fait prendre en train de publier des liens vers un site que les moderateurs estiment non-fiables vont voir leurs commentaires supprimés afin de garder la securité des gens sur ce subreddit!

**Nous allons garder ce thread mis a jour, alors revenez nous voir de temps en temps!**

**Si vous avez quelconque recommendation pour ce Mega Thread, veuillez Commenter! Les sites non-fiables seront supprimés pas les moderateurs.**

## **Traduction:**

Veuillez Consulter les **[traductions](https://github.com/taskylizard/piratedgames-megathread/blob/main/translations/translations.md)**.